/*
 * This is User service.
 */
export class UserService {
  userRepository: Repository<User> = getRepository(User);

  async findUserById(userId: number): Promise<User> {
    const loadedUser = await this.userRepository.findOne({ relations: ["activityLog"], where: userId });
    return loadedUser;
  }
}
